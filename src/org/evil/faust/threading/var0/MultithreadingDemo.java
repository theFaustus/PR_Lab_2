package org.evil.faust.threading.var0;

import java.util.concurrent.CountDownLatch;

/**
 * Created by Faust on 2/28/2017.
 */
public class MultithreadingDemo {
    public static void main(String[] args) {
        CountDownLatch headLatch = new CountDownLatch(2);

        new HeadThread(headLatch, "Thread-1").start();
        new HeadThread(headLatch, "Thread-2").start();
        new TailThread(headLatch, "Thread-3").start();

    }
}

class TailThread extends Thread {
    private final CountDownLatch latch;

    public TailThread(CountDownLatch latch, String name) {
        this.latch = latch;
        setName(name);
    }

    public void run() {
        System.out.println(Thread.currentThread().getName() + " waiting for signal...");
        try {
            latch.await();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        System.out.println(Thread.currentThread().getName() + " received the signal...");
    }
}

class HeadThread extends Thread {
    private final CountDownLatch latch;

    public HeadThread(CountDownLatch latch, String name) {
        this.latch = latch;
        setName(name);
    }

    public void run() {
        System.out.println(Thread.currentThread().getName() + " sending the signal...");
        latch.countDown();
        System.out.println(Thread.currentThread().getName() + " sent the signal...");
    }
}

