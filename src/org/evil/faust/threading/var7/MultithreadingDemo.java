package org.evil.faust.threading.var7;
import java.util.concurrent.*;

/**
 * Created by Faust on 2/27/2017.
 */
public class MultithreadingDemo {
    public static void main(String[] args) {
        CountDownLatch headLatch = new CountDownLatch(1);
        CountDownLatch topMiddle = new CountDownLatch(1);
        CountDownLatch bottomMiddle = new CountDownLatch(1);
        new HeadThread(headLatch, "Thread-1").start();
        new MiddleThread(headLatch, topMiddle, "Thread-2").start();
        new MiddleThread(headLatch, bottomMiddle, "Thread-3").start();
        new TailThread(topMiddle, "Thread-4").start();
        new TailThread(topMiddle, "Thread-5").start();
        new TailThread(bottomMiddle, "Thread-6").start();
        new TailThread(bottomMiddle, "Thread-7").start();
    }
}

class TailThread extends Thread {
    private final CountDownLatch latch;

    public TailThread(CountDownLatch latch, String name) {
        this.latch = latch;
        setName(name);
    }

    public void run() {
        System.out.println(Thread.currentThread().getName() + " waiting for signal...");
        try {
            latch.await();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        System.out.println(Thread.currentThread().getName() + " received the signal...");
    }
}

class HeadThread extends Thread {
    private final CountDownLatch latch;

    public HeadThread(CountDownLatch latch, String name) {
        this.latch = latch;
        setName(name);
    }

    public void run() {
        System.out.println(Thread.currentThread().getName() + " sending the signal...");
        latch.countDown();
        System.out.println(Thread.currentThread().getName() + " sent the signal...");
    }
}

class MiddleThread extends Thread {
    private final CountDownLatch waitLatch;
    private final CountDownLatch signalLatch;

    public MiddleThread(CountDownLatch waitLatch, CountDownLatch signalLatch, String name) {
        this.waitLatch = waitLatch;
        this.signalLatch = signalLatch;
        setName(name);
    }

    public void run() {
        System.out.println(Thread.currentThread().getName() + " waiting for signal...");
        try {
            waitLatch.await();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        System.out.println(Thread.currentThread().getName() + " received the signal...");
        System.out.println(Thread.currentThread().getName() + " sending the signal...");
        signalLatch.countDown();
        System.out.println(Thread.currentThread().getName() + " sent the signal...");
    }
}
