package org.evil.faust.threading.var6;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Semaphore;

/**
 * Created by Faust on 2/27/2017.
 */
public class MultithreadingDemo {
    public static void main(String[] args) {
        CountDownLatch ALatch = new CountDownLatch(1);
        CountDownLatch BLatch = new CountDownLatch(1);
        CountDownLatch CLatch = new CountDownLatch(1);
        CountDownLatch DLatch = new CountDownLatch(1);
        CountDownLatch ELatch = new CountDownLatch(1);


        new TailThread(DLatch, ELatch, "Thread-6").start();
        new SingleWaitAndSingleSendThread(ALatch, BLatch, "Thread-2").start();
        new DoubleWaitAndSingleSendThread(ALatch, BLatch, CLatch, "Thread-3").start();
        new DoubleWaitAndSingleSendThread(BLatch, CLatch, DLatch, "Thread-4").start();
        new SingleWaitAndSingleSendThread(DLatch, ELatch, "Thread-5").start();
        new HeadThread(ALatch, "Thread-1").start();

    }
}




