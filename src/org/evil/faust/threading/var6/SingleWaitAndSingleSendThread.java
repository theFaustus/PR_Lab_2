package org.evil.faust.threading.var6;

import java.util.concurrent.CountDownLatch;

/**
 * Created by Faust on 3/5/2017.
 */
public class SingleWaitAndSingleSendThread extends Thread {
    private final CountDownLatch waitLatch;
    private final CountDownLatch signalLatch;

    public SingleWaitAndSingleSendThread(CountDownLatch waitLatch, CountDownLatch signalLatch, String name) {
        this.waitLatch = waitLatch;
        this.signalLatch = signalLatch;
        setName(name);
    }

    public void run() {
        System.out.println(Thread.currentThread().getName() + " waiting for signal...");
        try {
            waitLatch.await();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }

        System.out.println(Thread.currentThread().getName() + " received the signal...");
        System.out.println(Thread.currentThread().getName() + " sending the signal...");
        signalLatch.countDown();
        System.out.println(Thread.currentThread().getName() + " sent the signal...");
    }

}

