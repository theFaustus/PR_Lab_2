package org.evil.faust.threading.var6;

import java.util.concurrent.CountDownLatch;

/**
 * Created by Faust on 3/5/2017.
 */
public class HeadThread extends Thread {
    private final CountDownLatch latch;

    public HeadThread(CountDownLatch latch, String name) {
        this.latch = latch;
        setName(name);
    }

    public void run() {
        System.out.println(Thread.currentThread().getName() + " sending the signal...");
        latch.countDown();
        System.out.println(Thread.currentThread().getName() + " sent the signal...");
    }
}

