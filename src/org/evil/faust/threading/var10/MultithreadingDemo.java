package org.evil.faust.threading.var10;

import java.util.concurrent.CountDownLatch;

/**
 * Created by Faust on 2/27/2017.
 */
public class MultithreadingDemo {
    public static void main(String[] args) {
        CountDownLatch secondLatch = new CountDownLatch(1);
        CountDownLatch headLatch = new CountDownLatch(1);
        CountDownLatch fourthLatch = new CountDownLatch(1);
        CountDownLatch fifthLatch = new CountDownLatch(1);
        CountDownLatch sixthLatch = new CountDownLatch(1);

        new HeadThread(headLatch, "Thread-3").start();
        new SingleWaitAndSingleSendThread(headLatch, secondLatch, "Thread-1").start();
        new DoubleWaitAndSingleSendThread(headLatch, secondLatch, fifthLatch, "Thread-2").start();
        new DoubleWaitAndSingleSendThread(headLatch, fifthLatch, fourthLatch, "Thread-5").start();
        new DoubleWaitAndSingleSendThread(headLatch, fourthLatch, sixthLatch, "Thread-4").start();
        new TailThread(headLatch, sixthLatch, "Thread-6").start();


    }
}

class TailThread extends Thread {
    private final CountDownLatch waitLatch;
    private final CountDownLatch waitLatch2;

    public TailThread(CountDownLatch waitLatch, CountDownLatch waitLatch2, String name) {
        this.waitLatch = waitLatch;
        this.waitLatch2 = waitLatch2;
        setName(name);
    }

    public void run() {
        System.out.println(Thread.currentThread().getName() + " waiting for signal...");
        try {
            waitLatch.await();
            waitLatch2.await();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        System.out.println(Thread.currentThread().getName() + " received the signal...");
    }
}

class HeadThread extends Thread {
    private final CountDownLatch latch;

    public HeadThread(CountDownLatch latch, String name) {
        this.latch = latch;
        setName(name);
    }

    public void run() {
        System.out.println(Thread.currentThread().getName() + " sending the signal...");
        latch.countDown();
        System.out.println(Thread.currentThread().getName() + " sent the signal...");
    }
}

class DoubleWaitAndSingleSendThread extends Thread {
    private final CountDownLatch waitLatch;
    private final CountDownLatch waitLatch2;
    private final CountDownLatch signalLatch;

    public DoubleWaitAndSingleSendThread(CountDownLatch waitLatch, CountDownLatch waitLatch2, CountDownLatch signalLatch, String name) {
        this.waitLatch = waitLatch;
        this.waitLatch2 = waitLatch2;
        this.signalLatch = signalLatch;
        setName(name);
    }

    public void run() {
        System.out.println(Thread.currentThread().getName() + " waiting for signal...");
        try {
            waitLatch.await();
            waitLatch2.await();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        System.out.println(Thread.currentThread().getName() + " received the signal...");
        System.out.println(Thread.currentThread().getName() + " sending the signal...");
        signalLatch.countDown();
        System.out.println(Thread.currentThread().getName() + " sent the signal...");
    }


}

class SingleWaitAndSingleSendThread extends Thread {
    private final CountDownLatch waitLatch;
    private final CountDownLatch signalLatch;

    public SingleWaitAndSingleSendThread(CountDownLatch waitLatch, CountDownLatch signalLatch, String name) {
        this.waitLatch = waitLatch;
        this.signalLatch = signalLatch;
        setName(name);
    }

    public void run() {
        System.out.println(Thread.currentThread().getName() + " waiting for signal...");
        try {
            waitLatch.await();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        System.out.println(Thread.currentThread().getName() + " received the signal...");
        System.out.println(Thread.currentThread().getName() + " sending the signal...");
        signalLatch.countDown();
        System.out.println(Thread.currentThread().getName() + " sent the signal...");
    }

}
