package org.evil.faust.threading.var8;

import java.util.concurrent.CountDownLatch;

/**
 * Created by Faust on 2/27/2017.
 */
public class MultithreadingDemo {
    public static void main(String[] args) {
        CountDownLatch headLatchTop = new CountDownLatch(2);
        CountDownLatch headLatchBottom = new CountDownLatch(2);
        CountDownLatch topMiddle = new CountDownLatch(1);
        CountDownLatch bottomMiddle = new CountDownLatch(1);

        new HeadThread(headLatchTop, "Thread-1").start();
        new HeadThread(headLatchTop, "Thread-2").start();
        new HeadThread(headLatchBottom, "Thread-3").start();
        new HeadThread(headLatchBottom, "Thread-4").start();

        new MiddleThread(headLatchTop, topMiddle, "Thread-5").start();
        new MiddleThread(headLatchBottom, bottomMiddle, "Thread-6").start();

        new TailThread(bottomMiddle, topMiddle, "Thread-7").start();

    }
}

class TailThread extends Thread {
    private final CountDownLatch bottomLatch;
    private final CountDownLatch topLatch;

    public TailThread(CountDownLatch bottomLatch, CountDownLatch topLatch, String name) {
        this.bottomLatch = bottomLatch;
        this.topLatch = topLatch;
        setName(name);
    }

    public void run() {
        System.out.println(Thread.currentThread().getName() + " waiting for signal...");
        try {
            bottomLatch.await();
            topLatch.await();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        System.out.println(Thread.currentThread().getName() + " received the signal...");
    }
}

class HeadThread extends Thread {
    private final CountDownLatch latch;

    public HeadThread(CountDownLatch latch, String name) {
        this.latch = latch;
        setName(name);
    }

    public void run() {
        System.out.println(Thread.currentThread().getName() + " sending the signal...");
        latch.countDown();
        System.out.println(Thread.currentThread().getName() + " sent the signal...");
    }
}

class MiddleThread extends Thread {
    private final CountDownLatch waitLatch;
    private final CountDownLatch signalLatch;

    public MiddleThread(CountDownLatch waitLatch, CountDownLatch signalLatch, String name) {
        this.waitLatch = waitLatch;
        this.signalLatch = signalLatch;
        setName(name);
    }

    public void run() {
        System.out.println(Thread.currentThread().getName() + " waiting for signal...");
        try {
            waitLatch.await();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        System.out.println(Thread.currentThread().getName() + " received the signal...");
        System.out.println(Thread.currentThread().getName() + " sending the signal...");
        signalLatch.countDown();
        System.out.println(Thread.currentThread().getName() + " sent the signal...");
    }
}
